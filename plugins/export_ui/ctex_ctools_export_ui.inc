<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'ctex_presets',  // As defined in hook_schema().
  'access' => 'administer ctex',  // Define a permission users must have to access these pages.

  // Define the menu item.
  'menu' => array(
    'menu item' => 'ctex',
    'menu title' => 'Chaos Tools Export UI Template',
    'menu description' => 'Administer ctex presets.',
  ),

  // Define user interface texts.
  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('Ctex preset'),
  'title plural proper' => t('Ctex presets'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'ctex_ctools_export_ui_form',
    // 'submit' and 'validate' are also valid callbacks.
  ),
);

/**
 * Implements HOOK_ctools_export_ui_form()
 */
function ctex_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];

    $form['field_one'] = array(
      '#type' => 'textfield',
      '#title' => t('Field one'),
      '#description' => t('The first field from your schema.'),
      '#default_value' => $preset->field_one,
      '#required' => true,
    );

    // Add Mymodule's configuration interface.
    $form['field_two'] = array(
      '#type' => 'textfield',
      '#title' => t('Field two'),
      '#description' => t('The second field from your scheme.'),
      '#default_value' => $preset->field_two,
      '#required' => true,
    );
}